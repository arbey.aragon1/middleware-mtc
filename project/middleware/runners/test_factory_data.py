import sys
sys.path.append('../storage-factory')

from storage_factory import StorageFactory as st_fa
import time
config={
    'fieldDataNames':['first_name', 'tt', 'last_name'],
    'fileDataName':'data.csv',
    'fieldOrderNames':['first_name', 'tt', 'last_name'],
    'fileOrderName':'orders.csv'
    }
test = st_fa.getInstance(config)

for i in range(1000):
    time.sleep(0.1)
    test.DATA_IN_SAVE({'first_name': 1, 'last_name': 'Beans', 'tt':3})
    test.BUY({'first_name': 5, 'last_name': 'Beans', 'tt':10})
    test.DATA_OUT_SAVE({'first_name': 8, 'last_name': 'Beans', 'tt':5})
    test.SELL({'first_name': 11, 'last_name': 'Beans', 'tt':4})

