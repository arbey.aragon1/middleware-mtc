import sys
sys.path.append('../algorithms')
sys.path.append('../shared')
sys.path.append('../simulations')
sys.path.append('../storage-factory')
sys.path.append('../market-factory')

from constants import LONG, SHORT, ANY, BUY, SELL
from storage_factory import StorageFactory as st_fa
from market_factory import MarketFactory

class SaveData(object):
    __config = None
    __sink = None
    
    __dao = None
    __market = None

    def __init__(self, config, sink, configDB = None, marketConfig = None):
        self.__config = config
        self.__market = MarketFactory.getInstance(marketConfig)
        self.__dao = st_fa.getInstance(configDB)

        self.__sink = sink \
            .do_action(lambda s: self.__saveDataInput(s)) \
            
    def __saveDataInput(self, s):
        self.__dao.DATA_IN_SAVE(s)

    def flowConnect(self):
        return self.__sink

market = MarketFactory.getInstance({})

configDB = {
    'fileDataName':'data.csv',
    'fileDataNameOut':'dataOut.csv',
    'fileOrderName':'orders.csv'
}
process = SaveData({'interval': 15}, 
    market.GET_STREAM({}), 
    configDB)

newFlow = process.flowConnect()

def func(s):
    print(s)

newFlow.subscribe(lambda s: func(s))
input("Press any key to quit\n")
