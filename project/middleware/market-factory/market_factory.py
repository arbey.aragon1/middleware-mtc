import sys
sys.path.append('../environments')

from environment import STAGING, PRODUCTION, SIMULATION, ENVIRONMENT
from market_default import MarketDefault
from market_production import MarketProduction
from market_simulation import MarketSimulation

class MarketFactory(object):
    @staticmethod
    def getInstance(config):
        if ENVIRONMENT == PRODUCTION:
            return MarketProduction.getInstance(config)
        elif ENVIRONMENT == SIMULATION:
            return MarketSimulation.getInstance(config)
        else:
            return MarketDefault.getInstance(config)
