from market_interface import MarketInterface

class MarketDefault(MarketInterface):
    __instance = None

    def __init__(self, config):
        if MarketDefault.__instance != None:
            raise Exception("Esta clase no es singleton!")
        else:
            MarketDefault.__instance = self

    @staticmethod
    def getInstance(config):
        if MarketDefault.__instance == None:
            MarketDefault(config)
        return MarketDefault.__instance 
    
    def GET_STREAM(self, data):
        pass

    def BUY(self, order):
        pass

    def SELL(self, order):
        pass