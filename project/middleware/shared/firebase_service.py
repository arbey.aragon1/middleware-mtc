from firebase.firebase import FirebaseApplication, FirebaseAuthentication
import time

class FirebaseService(object):
    __instance = None
    __firebase = None

    def __init__(self, config):
        if FirebaseService.__instance != None:
            raise Exception("Esta clase no es singleton!")
        else:
            FirebaseService.__instance = self
            DSN = 'https://mtcdb-912e6.firebaseio.com'
            FirebaseService.__firebase = FirebaseApplication(DSN, None)

    @staticmethod
    def getInstance(config):
        print("storage fb getIns")
        if FirebaseService.__instance == None:
            FirebaseService(config)
        return FirebaseService.__instance 
    
    def dataSet(self, url, name, data):
        result = FirebaseService.__firebase.put(url,
            name, 
            data=data, 
            params={'print': 'pretty'})
