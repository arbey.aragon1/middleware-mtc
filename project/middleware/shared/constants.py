import os

BUY, SELL = 2, 1
ANY, LONG, SHORT = 0, 2, 1

def makeDir(testDir):
    if not os.path.exists(testDir):
        os.makedirs(testDir)
    return testDir

PATH = os.getcwd().split('middleware')[0]+'middleware'
PATH_DATA = makeDir(PATH+'/data')
PATH_OUT_SIMULATION_DATA = makeDir(PATH+'/dataOut/simulation')
PATH_OUT_REAL_DATA = makeDir(PATH+'/dataOut/realData')
PATH_CREDENTIALS_OANDA = PATH+'/environments/config_V20.ini'