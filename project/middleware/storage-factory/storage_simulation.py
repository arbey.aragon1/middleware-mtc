import sys
sys.path.append('../shared')

import constants
from storage_interface import StorageInterface
from storage_simulation_save_files import StorageSimulationSaveFiles
import csv

class StorageSimulation(StorageInterface):
    __config = None
    __instance = None

    __dataSave = None
    __dataOutSave = None
    __orderSave = None
                
    def __init__(self, config):
        if StorageSimulation.__instance != None:
            raise Exception("Esta clase no es singleton!")
        else:
            StorageSimulation.__instance = self
            StorageSimulation.__config = config
            StorageSimulation.setup()

    @staticmethod
    def setup():
        configDataSave = {
            'fileDir': constants.PATH_OUT_SIMULATION_DATA + '/' + StorageSimulation.__config['fileDataName'],
            'loggerName':'data'
        }
        StorageSimulation.__dataSave = StorageSimulationSaveFiles(configDataSave)
        
        configDataOutSave = {
            'fileDir': constants.PATH_OUT_SIMULATION_DATA + '/' + StorageSimulation.__config['fileDataNameOut'],
            'loggerName':'dataOut'
        }
        StorageSimulation.__dataOutSave = StorageSimulationSaveFiles(configDataOutSave)

        configOrderSave = {
            'fileDir': constants.PATH_OUT_SIMULATION_DATA + '/' + StorageSimulation.__config['fileOrderName'],
            'loggerName':'orders'
        }
        StorageSimulation.__orderSave = StorageSimulationSaveFiles(configOrderSave)


    @staticmethod
    def getInstance(config):
        print("simulation save init")
        print(config)
        if StorageSimulation.__instance == None:
            StorageSimulation(config)
        return StorageSimulation.__instance 
    
    def DATA_IN_SAVE(self, data):
        StorageSimulation.__dataSave.saveRow(data)

    def DATA_OUT_SAVE(self, data):
        StorageSimulation.__dataOutSave.saveRow(data)

    def BUY(self, order):
        StorageSimulation.__orderSave.saveRow(order)

    def SELL(self, order):
        StorageSimulation.__orderSave.saveRow(order)

    def STATUS(self, structData):
        pass