from storage_interface import StorageInterface
import time
import sys
sys.path.append('../shared')
from firebase_service import FirebaseService

class StorageProduction(StorageInterface):
    __firebaseInstance = None
    __config = None
    __instance = None


    def __init__(self, config):
        if StorageProduction.__instance != None:
            raise Exception("Esta clase no es singleton!")
        else:
            StorageProduction.__instance = self
            self.__firebaseInstance = FirebaseService.getInstance({})

    @staticmethod
    def getInstance(config):
        print("storage getIns")
        if StorageProduction.__instance == None:
            StorageProduction(config)
        return StorageProduction.__instance 
    
    def __GET_STR_NAME(self):
        return str(int(round(time.time() * 1000)))

    def DATA_IN_SAVE(self, data):
        self.__firebaseInstance.dataSet('/historyInData', self.__GET_STR_NAME(), data)
        structData = {}
        structData['name'] = 'lastPriceData'
        structData['data'] = data
        self.STATUSUPDATE(structData)

    def DATA_OUT_SAVE(self, data):
        self.__firebaseInstance.dataSet('/historyOutData', self.__GET_STR_NAME(), data)
        
    def BUY(self, order):
        self.__firebaseInstance.dataSet('/historyOrders', self.__GET_STR_NAME(), order)
        structData = {}
        structData['name'] = 'lastOrder'
        structData['data'] = order
        self.STATUSUPDATE(structData)
    
    def SELL(self, order):
        self.__firebaseInstance.dataSet('/historyOrders', self.__GET_STR_NAME(), order)
        structData = {}
        structData['name'] = 'lastOrder'
        structData['data'] = order
        self.STATUSUPDATE(structData)
        
    def STATUSUPDATE(self, structData):
        self.__firebaseInstance.dataSet('/status', structData['name'], structData['data'])