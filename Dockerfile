FROM ubuntu:16.04
MAINTAINER ARBEY arbey.aragon@gmail.com

RUN apt-get update && apt-get install -y \
    wget ca-certificates \
    git curl vim python3-dev python3-pip \
    libfreetype6-dev libpng12-dev libhdf5-dev

RUN pip3 install --upgrade pip setuptools

RUN pip3 install numpy pandas seaborn pyyaml \
    h5py requests six oandapyV20 rx  \
    scipy pillow mglearn Flask  \
    gevent gunicorn h5py google-cloud-firestore python-firebase

RUN pip install --upgrade pip

CMD bash
